<?php

class UsersController extends MainController
{

	public $login_required = true;

	public function index() {
		
		$this->title = 'Users';
		
	
		if ( ! $this->logged_in ) {
		
			$this->logout();
			
			$this->goto_login();
			
			return;
		
		}
		
		$parametros = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();
	
        $modelo = $this->load_model('user-register/user-register-model');
				
		require ABSPATH . '/views/_includes/header.php';
			
        require ABSPATH . '/views/_includes/menu.php';
				
        require ABSPATH . '/views/users/user-list-view.php';
				
        require ABSPATH . '/views/_includes/footer.php';
		
    } 
	
} 