<?php

class DrinkController extends MainController
{

	public $login_required = true;

	public function index() {
		
		$this->title = 'Drink register';
		
	
		if ( ! $this->logged_in ) {
		
			$this->logout();
			
			$this->goto_login();
			
			return;
		
		}
		
		$parametros = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();
	
	    $modelo = $this->load_model('drink/drink-model');
				
		require ABSPATH . '/views/_includes/header.php';
			
        require ABSPATH . '/views/_includes/menu.php';
				
        require ABSPATH . '/views/drink/drink-view.php';
				
        require ABSPATH . '/views/_includes/footer.php';
		
    } 
	
} 