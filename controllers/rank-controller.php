<?php

class RankController extends MainController
{

	public $login_required = false;

	public function index() {
		
		$this->title = 'Ranking';
				
		$parametros = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();
	
        $modelo = $this->load_model('drink/drink-model');
				
		require ABSPATH . '/views/_includes/header.php';
			
        require ABSPATH . '/views/_includes/menu.php';
				
        require ABSPATH . '/views/rank/rank-view.php';
				
        require ABSPATH . '/views/_includes/footer.php';
		
    } 
	
} 