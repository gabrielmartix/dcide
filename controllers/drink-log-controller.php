<?php

class DrinkLogController extends MainController
{

	public $login_required = false;

	public function index() {
		
		$this->title = 'Drink register';
			
		$parametros = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();
	
	    $modelo = $this->load_model('drink/drink-model');
				
		require ABSPATH . '/views/_includes/header.php';
			
        require ABSPATH . '/views/_includes/menu.php';
				
        require ABSPATH . '/views/drink-log/drink-log-view.php';
				
        require ABSPATH . '/views/_includes/footer.php';
		
    } 
	
} 