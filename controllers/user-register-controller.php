<?php

class UserRegisterController extends MainController
{

	public $login_required = false;

	public function index() {
		
		$this->title = 'User Register';
						
		$parametros = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();
	
		$modelo = $this->load_model('user-register/user-register-model');
						
		require ABSPATH . '/views/_includes/header.php';
				
        require ABSPATH . '/views/_includes/menu.php';
		
		require ABSPATH . '/views/user-register/user-register-view.php';
		
	    require ABSPATH . '/views/_includes/footer.php';
		
    } 
	
} 