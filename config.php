<?php

// Caminho para a raiz
define( 'ABSPATH', dirname( __FILE__ ) );

define( 'HOME_URI', 'http://127.0.0.1/dcide' );

define( 'HOSTNAME', 'localhost' );

define( 'DB_NAME', 'dcide' );

define( 'DB_USER', 'root' );

define( 'DB_PASSWORD', '' );

define( 'DB_CHARSET', 'utf8' );

define( 'DEBUG', false );

// Carrega o loader, que vai carregar a aplicação inteira
require_once ABSPATH . '/loader.php';
?>