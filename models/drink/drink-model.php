<?php 

class DrinkModel
{

	public $form_data;

	public $form_msg;

	public $db;

	public function __construct( $db = false ) {
		$this->db = $db;
	}

	public function validate_drink_form () {
	
		$this->form_data = array();
		
		if ( 'POST' == $_SERVER['REQUEST_METHOD'] && ! empty ( $_POST ) ) {
		
			foreach ( $_POST as $key => $value ) {
			
				$this->form_data[$key] = $value;
				
				if ( empty( $value ) ) {
					
					$this->form_msg = '<p class="form_error">There are empty field. Data has not been sent.</p>';
					
					return;
					
				}			
			
			}
		
		} else {
		
			return;
			
		}
		
		if( empty( $this->form_data ) ) {
			return;
		}
        		
        $userdata = $_SESSION['userdata'];
        
		$query = $this->db->insert('consumo', array(
            
            'volume' => chk_array( $this->form_data, 'volume'), 
            'user_id' => chk_array( $userdata, 'user_id'),
            'data' => date("Y-m-d H:i:s")
        ));
    } 
    
    public function get_drink_list() {
	
		$query = $this->db->query('SELECT * FROM `consumo` ORDER BY `data` DESC');
		
		if ( ! $query ) {
			return array();
		}
	
		return $query->fetchAll();
    } 
    
    public function get_rank_list() {

        date_default_timezone_set('America/Sao_Paulo');
        $hj = date("Y-m-d");
            
        $hora1 = $hj.' 00:00:00';
        $hora2 = $hj.' 23:59:59';
	
		$query = $this->db->query('SELECT users.user_id, user_name, data, SUM(volume) as total FROM consumo INNER JOIN users ON(users.user_id = consumo.user_id)  WHERE data Between "'.$hora1.'" and "'.$hora2.'"  group by user_id order by total DESC');
		
		if ( ! $query ) {
			return array();
		}
	
		return $query->fetchAll();
	} 
	
}