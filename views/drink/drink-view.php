<?php if ( ! defined('ABSPATH')) exit; ?>

<div class="wrap">
    <p><b>Registre o Volume de água consumido</b></p>
<?php

$modelo->validate_drink_form();

?>

<form method="post" action="">
	<table class="form-table">
		
		<tr>
			<td>Volume: </td>
			<td> <input type="text" name="volume" value="<?php
				echo htmlentities( chk_array( $modelo->form_data, 'volume') );
			?>" /></td>
        </tr>
        <tr>
			<td colspan="2">
				<?php echo $modelo->form_msg;?>
				<input type="submit" value="Registrar Consumo" />
				<a href="<?php echo HOME_URI . '/drink';?>">Novo Registro</a>
			</td>
		</tr>
		
	</table>
</form>