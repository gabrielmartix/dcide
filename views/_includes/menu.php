<?php if ( ! defined('ABSPATH')) exit; ?>

<?php if ( $this->login_required && ! $this->logged_in ) return; ?>

<nav class="menu clearfix">
	<ul>
		<li><a href="<?php echo HOME_URI;?>">Home</a></li>
		<li><a href="<?php echo HOME_URI;?>/login/">Login</a></li>
		<li><a href="<?php echo HOME_URI;?>/user-register/">User Register</a></li>
		<li><a href="<?php echo HOME_URI;?>/users/">User list</a></li>
		<li><a href="<?php echo HOME_URI;?>/drink/">Drink Register</a></li>
		<li><a href="<?php echo HOME_URI;?>/rank/">Ranking</a></li>
		<li><a href="<?php echo HOME_URI;?>/drink-log/">Drink Log</a></li>
		
	</ul>
</nav>