<?php if ( ! defined('ABSPATH')) exit; ?>

<div class="wrap">

	<p><b>Escopo do Sistema</b></p>
		<p>Você foi convidado para desenvolver a API de um projeto. Para isso, foi escrito uma
		pequena documentação das funcionalidades necessárias no projeto. Os desenvolvedores
		frontend usarão a sua API para criar um aplicativo pessoal para monitorar quantas vezes o
		usuário bebeu água.</p>

	<p>Observações de implementação:</br>
	- O projeto deve ser desenvolvido em PHP e com banco de dados relacional;</br>
	- Não deve ser utilizado nenhum framework (Laravel, Slim framework, Doctrine, etc.);</br>
	- Se possível, a API deve seguir o padrão REST;</br>
	- É desejável que o código use o método Programação Orientada a Objetos;</p>

	<p><b>Tratamentos desejáveis:</b></br>
- Na criação de um usuário, retornar um erro se o usuário já existe</br>
- No login, alertar que o usuário não existe ou que a senha está inválida</br>
- Na edição e na remoção do usuário, limitar-se apenas ao usuário autenticado</br>

<b>Tratamentos opcionais:</b></br>

	- Criar um serviço que liste o histórico de registros de um usuário (retornando a data e
	a quantidade em mL de cada registro)</br>
	- Criar um serviço que liste o ranking do usuário que mais bebeu água hoje
	(considerando os ml e não a quantidade de vezes), retornando o nome e a
	quantidade em mililitros (mL)</p>

</div> <!-- .wrap -->