<?php if ( ! defined('ABSPATH')) exit; ?>

<div class="wrap">

<?php 
// Lista os usuários
$lista = $modelo->get_drink_list(); 
?>

<table class="list-table">
	<thead>
		<tr>
			<th>Data</th>
			<th>Volume(ml)</th>
			
		</tr>
	</thead>
			
	<tbody>
			
		<?php foreach ($lista as $fetch_userdata): ?>

			<tr>
			
				<td> <?php echo $fetch_userdata['data'] ?> </td>
				<td> <?php echo $fetch_userdata['volume'] ?> </td>
			
			</tr>
			
		<?php endforeach;?>
			
	</tbody>
</table>

</div> <!-- .wrap -->
