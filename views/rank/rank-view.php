<?php if ( ! defined('ABSPATH')) exit; ?>

<div class="wrap">

<?php 
// Lista os usuários
$lista = $modelo->get_rank_list(); 
?>
<h3> Rank de Hoje</h3>
<table class="list-table">
	<thead>
		<tr>
			<th>ID</th>
			<th>Name</th>
			<th>Volume</th>
		</tr>
	</thead>
			
	<tbody>
			
		<?php foreach ($lista as $fetch_userdata): ?>

			<tr>
			
				<td> <?php echo $fetch_userdata['user_id'] ?> </td>
				<td> <?php echo $fetch_userdata['user_name'] ?> </td>
                <td> <?php echo $fetch_userdata['total'] ?> </td>
             
			</tr>
			
		<?php endforeach;?>
			
	</tbody>
</table>

</div> <!-- .wrap -->
